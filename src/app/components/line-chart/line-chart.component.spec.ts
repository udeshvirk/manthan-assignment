import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LineChartService } from './service/line-chart.service';
import { LineChartComponent } from './line-chart.component';
import { of } from 'rxjs';
import { FormsModule } from '@angular/forms';

describe('LineChartComponent', () => {
    let component: LineChartComponent;
    let fixture: ComponentFixture<LineChartComponent>;

    beforeEach(async(() => {
        const LineChartServiceSpy: jasmine.SpyObj<LineChartService> = jasmine.createSpyObj('LineChartService',
            [
                'getReportData'
            ]);
        LineChartServiceSpy.getReportData.and.returnValue(of([]));
        TestBed.configureTestingModule({
            imports:[FormsModule],
            declarations: [
                LineChartComponent
            ],
            providers: [
                { provide: LineChartService, useValue: LineChartServiceSpy }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LineChartComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    afterEach(() => {
        fixture.destroy();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

});
