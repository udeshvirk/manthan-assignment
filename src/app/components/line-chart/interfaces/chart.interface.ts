export interface ICHART {
    title?: string;
    type: string;
    data?: Array<Array<string|number>>;
    columnNames: Array<string>;
    width: number;
    height: number;
}
