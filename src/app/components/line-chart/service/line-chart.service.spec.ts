import { HttpClientModule } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';
import { of, throwError } from 'rxjs';
import { LineChartService } from './line-chart.service';

describe('LineChartService', () => {
    let service: LineChartService;
    beforeEach(() => {

        TestBed.configureTestingModule({
            imports: [HttpClientModule],
            providers: [
                LineChartService
            ]
        });
        service = TestBed.inject(LineChartService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
