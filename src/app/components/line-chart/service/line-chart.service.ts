import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class LineChartService {

    constructor(private http: HttpClient) { }

    // get report data from json
    getReportData(): Observable<any> {
        const url = '/assets/mocks/report.json';
        return this.http.get(url);
    }
}
