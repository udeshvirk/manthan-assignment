import { ICHART_METRIC } from "../interfaces/chart-metric.interface";

const CHART_METRIC: Array<ICHART_METRIC> = [
    {
        title: 'Overall Sales',
        name: 'Sales',
        key: 'sales'
    },
    {
        title: 'Overall Orders',
        name: 'Orders',
        key: 'orders'
    },
    {
        title: 'Page Views',
        name: 'Page Views',
        key: 'pageViews'
    },
    {
        title: 'Click Through Rate',
        name: 'Page rec clickthru rate',
        key: 'clickThruRate'
    }
]
export default CHART_METRIC;