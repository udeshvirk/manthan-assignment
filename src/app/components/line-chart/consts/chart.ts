import { ICHART } from "../interfaces/chart.interface";

const CHART: ICHART = {
    type: 'LineChart',
    columnNames: ['Date'],
    width: 1080,
    height: 500,
}
export default CHART;