import { Component, OnDestroy, OnInit } from '@angular/core';
import CHART from './consts/chart';
import CHART_METRIC from './consts/chart-metric';
import { ICHART_METRIC } from './interfaces/chart-metric.interface';
import { ICHART } from './interfaces/chart.interface';
import { LineChartService } from './service/line-chart.service';

@Component({
    selector: 'app-line-chart',
    templateUrl: './line-chart.component.html',
    styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent implements OnInit, OnDestroy {

    _subscriptions: Array<any> = [];
    reportData = [];
    chartData: ICHART = CHART;
    chartMetrics: Array<ICHART_METRIC> = CHART_METRIC;
    selectedMetric: ICHART_METRIC = this.chartMetrics[0];

    constructor(private service: LineChartService) { }

    ngOnInit(): void {
        this.getReportData();

    }

    //on metric value change
    changeMetric() {
        this.setMetricData();
    }

    //Set chart data for selected metric
    setMetricData() {
        this.chartData.data = this.reportData.map((record) => {
            return [record['date'], record[this.selectedMetric.key]];
        });
        this.chartData.columnNames[1] = this.selectedMetric.title;
    }

    // get report data from json
    getReportData() {
        this._subscriptions.push(
            this.service.getReportData()
                .subscribe(
                    (data) => {
                        this.reportData = data.records;
                        this.setMetricData();
                    }
                )
        );
    }

    ngOnDestroy() {
        // destroy subscriptions
        this._subscriptions.forEach((item) => {
            if (item) {
                item.unsubscribe();
            }
        });
    }

}
