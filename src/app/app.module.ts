import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LineChartComponent } from './components/line-chart/line-chart.component';
import { LineChartService } from './components/line-chart/service/line-chart.service';
import { GoogleChartsModule } from 'angular-google-charts';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        AppComponent,
        LineChartComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        GoogleChartsModule
    ],
    providers: [LineChartService],
    bootstrap: [AppComponent]
})
export class AppModule { }
